package com.BookMyfurniture.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;

import com.BookMyfurniture.commonUtils.commonLibrary;

public class HomePage extends commonLibrary {
	
	static final Logger logger = Logger.getLogger(HomePage.class);	

   private final  By SIGN_IN = By.xpath("//*[@id=\"navbarSupportedContent\"]/form/button[4]");
   
   private final  By USER = By.tagName("/html/body/app-root/bmf-layout/div[1]/nav/div/form/button[4]/span");
     
   private final By CHAIRIMGHOME = By.xpath("/html/body/app-root/bmf-layout/div[2]/app-homepage/div/home-category-list/div[1]/div/div/div[1]/div/div");
   
	private final By CHAIRIMGBOOKING = By.xpath("/html/body/app-root/bmf-layout/div[2]/product-catalogue/div/div/div/div[3]/div[2]/div/div[1]/div/button/span/img");

	private final By ALLFURNITURE = By.xpath("/html/body/app-root/bmf-layout/div[1]/nav/button[1]");
	
   
	
    public  boolean clickLink(String button) {
    	logger.info("Inside click");
    	click(button,SIGN_IN);
    	return true;
    }
    
    public  boolean click(String button) {
    	logger.info("Inside click");
    	if(button.contains("SignIn")) {
    		click(button,SIGN_IN);
    		return true;
    	}else if(button.contains("ALLFURNITURE")){
    		click(button,ALLFURNITURE);
    	}
    	return false;
    }
    
    public  boolean clickChairLink(String button) {
    	logger.info("Inside click");
    	click(button,CHAIRIMGHOME);
    	return true;
    }
    
    public  boolean clickChair(String button) {
    	logger.info("Inside click");
    	click(button,CHAIRIMGBOOKING);
    	return true;
    }
    
    public boolean verifyUser(String name) throws InterruptedException {
    	logger.info("inside");
    	Thread.sleep(3);
    	logger.info("name"+name);
    	logger.info("USER"+USER);
    	logger.info("USER"+USER.tagName("<span>"));
    	logger.info("USER ==="+USER.equals(name));
    	if(name.equals(USER)) {
    		logger.info("name"+name);
    		logger.info("USER ==="+USER);
    		return true;
    	}
    	return true;
    }
}


