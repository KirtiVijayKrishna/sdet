package com.BookMyfurniture.pages;

import org.openqa.selenium.By;

import com.BookMyfurniture.commonUtils.commonLibrary;

public class RegisterPage extends commonLibrary {
	
	private final By FULLNAME = By.xpath("//*[@id='name']");
	
	//private final By FULLNAME = By.xpath("/html/body/app-root/bmf-layout/div[2]/app-login/div/p-sidebar/div/app-signup/div/div/form/div[1]/input");
	
	private final By MOBILENO = By.xpath("//*[@id='mobileNo']");
	
	//private final By EMAIL = By.xpath("//*[@id='emailId']");
	
	private final By EMAIL = By.xpath("/html/body/app-root/bmf-layout/div[2]/app-login/div/div/div[2]/div[1]/form/div[1]/input");
	
	private final By PASSWORD = By.xpath("//*[@id='password']");
	
	private final By REGISTER = By.xpath("/html/body/app-root/bmf-layout/div[2]/app-login/div/p-sidebar/div/app-signup/div/div/form/div[5]/div[1]/button");
	
    
    public boolean fillForm(String name,String mobileNo,String email,String password,String submit) {
    	try {
    		enterText(name,FULLNAME);
    		enterText(mobileNo,MOBILENO);
    		enterText(email,EMAIL);
    		enterText(password,PASSWORD);
    		submit(REGISTER);
    		return true;
    	}catch(Exception e) { e.getMessage();}
		return false;
    }
    
    public boolean fillForm(String email,String password) {
    	try {
    		enterText(email,EMAIL);
    		enterText(password,PASSWORD);
    		return true;
    	}catch(Exception e) { e.getMessage();}
		return false;
    }
    

}
