package com.BookMyfurniture.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import com.BookMyfurniture.commonUtils.commonLibrary;

public class FurnitureCategory extends commonLibrary {
	
	static final Logger logger = Logger.getLogger(FurnitureCategory.class);
	
	 private final  By CHAIR = By.xpath("/html/body/app-root/bmf-layout/div[2]/product-catalogue/div/div/div/div[3]/div[1]/form/ul[1]/li[1]/mat-checkbox");
	 
	 private final By SOFA = By.xpath("/html/body/app-root/bmf-layout/div[2]/product-catalogue/div/div/div/div[3]/div[1]/form/ul[1]/li[2]/mat-checkbox");
	 
	 private final By PRICEABOVE  = By.xpath("/html/body/app-root/bmf-layout/div[2]/product-catalogue/div/div/div/div[3]/div[1]/form/ul[2]/li[3]/mat-checkbox/label/span");
	 
	 private final By CHAIRIMGBOOKING = By.xpath("/html/body/app-root/bmf-layout/div[2]/product-catalogue/div/div/div/div[3]/div[2]/div[2]/div[1]/div/button/span/img");
	 
	 
	 public boolean click(String button) {
		 logger.info("Inside click");
	    	if(button.equals("SOFA")) {
	    		click(button,SOFA);
	    		return true;
	    	}else if(button.equals("PRICE")) {
	    		click(button,PRICEABOVE);
		    	return true;
	    	}
	 		else if(button.equals("CHAIRIMG")) {
	 			click(button,CHAIRIMGBOOKING);
	 			return true;
	 		} 
	    	return false;
	  }
	 
}
	 
	//*[@id="mat-checkbox-65"]/label/div
	 
	/* public boolean click(String button) {
	    	Logger.info("Inside click");
	    	click(button,PRICEABOVE);
	    	return true;
	  } */
	 
	 
