package com.BookMyfurniture.pages;


import org.openqa.selenium.By;
import com.BookMyfurniture.commonUtils.commonLibrary;

public class LoginPage extends commonLibrary {
	
	private final By CREATEACCOUNT = By.xpath("/html/body/app-root/bmf-layout/div[2]/app-login/div/div/div[2]/div[3]/h3/div/div[2]/button");
	
	private final By FULLNAME = By.xpath("//*[@id='name']");
	
	//private final By FULLNAME = By.xpath("/html/body/app-root/bmf-layout/div[2]/app-login/div/p-sidebar/div/app-signup/div/div/form/div[1]/input");
	
	private final By MOBILENO = By.xpath("//*[@id='mobileNo']");
	
	//private final By EMAIL = By.xpath("//*[@id='emailId']");
	
	private final By EMAIL = By.xpath("/html/body/app-root/bmf-layout/div[2]/app-login/div/div/div[2]/div[1]/form/div[1]/input");
	
	private final By PASSWORD = By.xpath("//*[@id='password']");
	
	
	
	private final By REGISTER = By.xpath("/html/body/app-root/bmf-layout/div[2]/app-login/div/p-sidebar/div/app-signup/div/div/form/div[5]/div[1]/button");
	
	String emailId;
	String password;

	public LoginPage(String emailId, String password){
		this.emailId= emailId;
		this.password =password;
	}

	public  void click(String button) {
    	click(button,CREATEACCOUNT);
    }
    
    public boolean fillForm(String name,String mobileNo,String email,String password,String submit) {
    	try {
    		enterText(name,FULLNAME);
    		enterText(mobileNo,MOBILENO);
    		enterText(email,EMAIL);
    		enterText(password,PASSWORD);
    		submit(REGISTER);
    		return true;
    	}catch(Exception e) { e.getMessage();}
		return false;
    }
    
    public boolean fillForm(String email,String password) {
    	try {
    		enterText(email,EMAIL);
    		enterText(password,PASSWORD);
    		return true;
    	}catch(Exception e) { e.getMessage();}
		return false;
    }
    
    
}

