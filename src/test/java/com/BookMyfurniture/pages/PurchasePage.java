package com.BookMyfurniture.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import com.BookMyfurniture.commonUtils.commonLibrary;

public class PurchasePage extends commonLibrary {
	
	static final Logger logger = Logger.getLogger(PurchasePage.class);
	private final By CONFIRMYES = By.xpath("//*[@id='confirmBox']/p-confirmdialog/div/div[3]/button[1]/span[2]");
	private final By MODECASH = By.xpath("/html/body/app-root/bmf-layout/div[2]/app-payment-layout/div/div/div[1]/div[3]/p-radiobutton/label");
	private final By PAYMENT = By.xpath("/html/body/app-root/bmf-layout/div[2]/app-payment-layout/div/div/div[2]/button");
	
	 public  boolean click(String button) {
		 	logger.info("Inside click");
	    	if(button.contains("MODECASH")) {
	    		click(button,MODECASH);
	    		return true;
	    	}else if(button.contains("PAYMENT")){
	    		click(button,PAYMENT);
	 		}else if(button.contains("CONFIRMYES")){
	 			click(button,CONFIRMYES);
	 		}
	    	return false;
	    }
	
}
