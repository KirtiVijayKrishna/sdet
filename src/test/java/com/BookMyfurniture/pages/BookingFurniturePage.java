package com.BookMyfurniture.pages;

import org.openqa.selenium.By;
import com.BookMyfurniture.commonUtils.commonLibrary;


public class BookingFurniturePage extends commonLibrary {
	
	
	private final By BOOKNOW = By.xpath("/html/body/app-root/bmf-layout/div[2]/selected-product/div/div[1]/div[1]/div[2]/div[1]/div[2]/button");
	private final By PLACEORDER = By.xpath("/html/body/app-root/bmf-layout/div[2]/single-order-confirm/div/div/div[2]/div[3]/div[2]/tr[4]/td/button");
	
	
	
	//private final By BOOKINGCHAIR = By.xpath("/html/body/app-root/bmf-layout/div[2]/app-homepage/div/home-category-list/div[1]/div/div/div[1]/div/div/span/svg");
	
	 public  boolean click(String button) {
	    	logger.info("Inside click");
	    	click(button,BOOKNOW);
	    	return true;
	 }
	
	 public  boolean clickPlaceOrder(String button) {
	    	logger.info("Inside click");
	    	click(button,PLACEORDER);
	    	return true;
	 }

	/* public  boolean confirmation(String button) {
	    	logger.info("Inside click");
	    	click(button,CONFIRMYES);
	    	return true;
	 }

*/
	 }

