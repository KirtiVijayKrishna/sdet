package com.BookMyfurniture.rest.tests;


	import org.apache.log4j.Logger;
	import org.json.JSONObject;
	import org.testng.annotations.Test;

	import com.BookMyfurniture.commonUtils.RestBase;
	import com.BookMyfurniture.commonUtils.RestController;

	import io.restassured.RestAssured;
	import io.restassured.http.Method;
	import io.restassured.response.Response;

	public class ProfileAuthenticationPostTest extends RestBase {
		
		public static final Logger logger = Logger.getLogger(ProfileAuthenticationPostTest.class);
		
		@Test
		public void PostUserLogin(String emailId, String password) {
			//password,userID
			RestAssured.baseURI = "http://okmry52647dns.eastus.cloudapp.azure.com:8089";
			JSONObject user = new JSONObject();
			user.put("emailId", emailId);
			user.put("password", password);
			String token=  RestController.generateAccessToken(emailId, password);
			response = (Response) RestAssured.given().header("Authorization", "Bearer "+token+",Content-Type", "application/json").body(user.toString()).request(Method.POST, "/rest/api/profile/authenticate");
			logger.info("response thtrough rest api ===" + response);
		}

	}

