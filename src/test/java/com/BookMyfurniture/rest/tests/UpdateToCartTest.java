package com.BookMyfurniture.rest.tests;

	import java.sql.Connection;
	import java.sql.ResultSet;
	import java.sql.Statement;

	import org.apache.log4j.Logger;
	import org.json.JSONObject;
	import org.testng.annotations.Test;

	import com.BookMyfurniture.commonUtils.Dao;
	import com.BookMyfurniture.commonUtils.RestBase;
	import com.BookMyfurniture.commonUtils.commonLibrary;
	import com.BookMyfurniture.commonUtils.RestController;

	import io.restassured.RestAssured;
	import io.restassured.http.Method;
	import io.restassured.response.Response;
	import junit.framework.Assert;

	public class UpdateToCartTest extends RestBase {

		public static final Logger logger = Logger.getLogger(UpdateToCartTest.class);

		String emailId;
		String password;
		int userId = 323;

		@Test
		public void UpdateItem() {
			RestAssured.baseURI = commonLibrary.restUrl;
			JSONObject cardId = new JSONObject();
			JSONObject product = new JSONObject();
			JSONObject categoryId = new JSONObject();
			JSONObject profile = new JSONObject();
			JSONObject role = new JSONObject();
			
			cardId.put("cartId", 258);
			product.put("pid", 3);
			product.put("name", "Cambridge chair");
			categoryId.put("id", 2);
			categoryId.put("name", "chair");
			categoryId.put("description", "chair to sit");
			product.put("categoryId", categoryId);
			
			product.put("color", "yellow");
			product.put("price", 14000);
			product.put("warranty", 1);
			product.put("availability", true);
			cardId.put("product", product);
			
			profile.put("userId",userId);
			profile.put("name","john");
			product.put("discount", 10);
			
			role.put("roleId",2);
			role.put("name","user");
			role.put("description","Has customer access");
			profile.put("role", role);
			cardId.put("profile", profile);
			cardId.put("quantity", 3);	
			
			UserLoginTest user = new UserLoginTest(emailId, password);
			String token = RestController.generateAccessToken(emailId, password);
			response = (Response) RestAssured.given().header("Content-Type", "application/json")
					.header("Authorization", "Bearer " + token).body(cardId.toString())
					.request(Method.PUT, "/rest/api/cart/");
			System.out.println("response thtrough rest api ===" + response);
			//logger.info("response thtrough rest api ===" + response);
		}

		@Test
		public void validateResponse() {
			try {
				String resp = response.getBody().asString();
				int statusCode = response.getStatusCode();
				Assert.assertEquals(resp, "Successfully updated quantity of the product");
				Assert.assertEquals(statusCode, 200);
				System.out.println("response Updating item  ===" + statusCode);
				System.out.println("response adding item  ===" + resp);
			} catch (Exception e) {
				e.getMessage();
				logger.error("Quantity of the product has been updated successfully");
			}
		}

		@Test
		public boolean verifyItemAddedToCart() {
			Statement stmt = null;
			ResultSet rs = null;
			try {
				Connection con = Dao.connect();
				stmt = con.createStatement();
				logger.info("emailId ========" + emailId);
				rs = stmt.executeQuery("select card_id  from cart  where product_id  = 2 ");
				while (rs.next()) {
					if (rs.getInt(1) > 0) {
						logger.info("Record present");
						return true;
					}
				}

			} catch (Exception e) {
				e.getMessage();
			}
			return false;

		}
	}


