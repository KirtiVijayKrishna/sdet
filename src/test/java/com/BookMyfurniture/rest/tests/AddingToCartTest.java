package com.BookMyfurniture.rest.tests;

	import java.sql.Connection;
	import java.sql.ResultSet;
	import java.sql.Statement;

	import org.apache.log4j.Logger;
	import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

	import com.BookMyfurniture.commonUtils.Dao;
	import com.BookMyfurniture.commonUtils.RestBase;
	import com.BookMyfurniture.commonUtils.commonLibrary;
	import com.BookMyfurniture.commonUtils.RestController;
	import com.BookMyfurniture.ui.tests.LoginTest;

	import io.restassured.RestAssured;
	import io.restassured.http.Method;
	import io.restassured.response.Response;

	public class AddingToCartTest extends RestBase {

		public static final Logger logger = Logger.getLogger(AddingToCartTest.class);

		String emailId="peter@gmail.com";
		String password;

		/*public AddingToCartTest(String emailId, String password) {
			//this.emailId = emailId;
			//this.password = password;

		}*/

		@Test
		public void addItem() {
			RestAssured.baseURI = commonLibrary.restUrl;
			JSONObject item = new JSONObject();
			JSONObject categoryId = new JSONObject();
			item.put("pid", 3);
			item.put("name", "Cambridge chair");
			categoryId.put("id", 2);
			categoryId.put("name", "chair");
			categoryId.put("description", "chair to sit");
			item.put("categoryId", categoryId);
			item.put("color", "yellow");
			item.put("price", 12000);
			item.put("warranty", 1);
			item.put("availability", true);
			item.put("discount", 10);
			UserLoginTest user = new UserLoginTest(emailId, password);
			String token = RestController.generateAccessToken(emailId, password);
			response = (Response) RestAssured.given().header("Content-Type", "application/json")
					.header("Authorization", "Bearer " + token).body(item.toString())
					.request(Method.POST, "/rest/api/cart/user/273");
			logger.info("response thtrough rest api ===" + response);
		}

		@Test
		public void validateResponse() {
			try {
				String resp = response.getBody().asString();
				int statusCode = response.getStatusCode();
				Assert.assertEquals(resp, 201);
				logger.info("response adding item  ===" + statusCode);
				logger.info("response adding item  ===" + resp);
			} catch (Exception e) {
				e.getMessage();
				logger.error("Product already exists in cart");
			}
		}

		@Test
		public boolean verifyItemAddedToCart() {
			Statement stmt = null;
			ResultSet rs = null;
			try {
				Connection con = Dao.connect();
				stmt = con.createStatement();
				logger.info("emailId ========" + emailId);
				rs = stmt.executeQuery("select count(*) from product where email_id='" + emailId + "'");
				
				//select cart_id  from cart where product_id = 2 and profile_user_id =117
				
						while (rs.next()) {
					if (rs.getInt(1) > 0) {
						logger.info("Record present");
						return true;
					}
				}

			} catch (Exception e) {
				e.getMessage();
			}
			return false;

		}
	}

	
