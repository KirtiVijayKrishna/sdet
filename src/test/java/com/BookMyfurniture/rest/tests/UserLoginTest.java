package com.BookMyfurniture.rest.tests;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.BookMyfurniture.commonUtils.Dao;
import com.BookMyfurniture.commonUtils.RestBase;
import com.BookMyfurniture.commonUtils.commonLibrary;
import com.BookMyfurniture.commonUtils.RestController;
import com.BookMyfurniture.ui.tests.LoginTest;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;

public class UserLoginTest extends RestBase {

	public static final Logger logger = Logger.getLogger(LoginTest.class);

	Response response;
	String emailId;
	String password;

	public UserLoginTest(String emaildId, String password) {
		this.emailId = emaildId;
		this.password = password;
	}

	@Test
	public void postUserLogin(String emailId, String password) {
		RestAssured.baseURI = commonLibrary.restUrl;
		JSONObject user = new JSONObject();
		user.put("emailId", emailId);
		user.put("password", password);
		String token = RestController.generateAccessToken(emailId, password);
		response = (Response) RestAssured.given()
				.header("Authorization", "Bearer " + token + ",Content-Type", "application/json").body(user.toString())
				.request(Method.POST, "/rest/api/login/signin");
		logger.info("response thtrough rest api ===" + response);
	}

	@Test
	public void validateResponse() {
		String resp = response.getBody().asString();
		int statusCode = response.getStatusCode();
		Assert.assertEquals(statusCode, 201);
		logger.info("response user login in through Rest api ===" + statusCode);
	}

	@Test
	public boolean verifyUser() throws SQLException, ClassNotFoundException {
		Statement stmt = null;
		ResultSet rs = null;
		try {
			Connection con = Dao.connect();
			stmt = con.createStatement();
			logger.info("emailId ========" + emailId);
			rs = stmt.executeQuery("select count(*) from profile where email_id='" + emailId + "'");
			while (rs.next()) {
				if (rs.getInt(1) > 0) {
					logger.info("Record present");
					return true;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Test
	public boolean getUserId() throws SQLException, ClassNotFoundException {
		Statement stmt = null;
		ResultSet rs = null;
		try {
			Connection con = Dao.connect();
			stmt = con.createStatement();
			logger.info("emailId ========" + emailId);
			rs = stmt.executeQuery("select userId from profile where email_id='" + emailId + "'");
			while (rs.next()) {
				logger.info("Record present" + rs.getString(1));
				if (rs.getString(1) != null) {
					logger.info("User id " + rs.getString(1));
					return true;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
}
