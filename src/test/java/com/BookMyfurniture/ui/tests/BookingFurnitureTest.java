package com.BookMyfurniture.ui.tests;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;
import com.BookMyfurniture.commonUtils.RestController;
import com.BookMyfurniture.commonUtils.commonLibrary;
import com.BookMyfurniture.pages.BookingFurniturePage;
import com.BookMyfurniture.pages.FurnitureCategory;
import com.BookMyfurniture.pages.HomePage;
import com.BookMyfurniture.pages.LoginPage;
import com.BookMyfurniture.pages.PurchasePage;
import com.BookMyfurniture.rest.tests.AddingToCartTest;
import com.BookMyfurniture.rest.tests.UpdateToCartTest;
import com.BookMyfurniture.rest.tests.UserLoginTest;

public class BookingFurnitureTest extends commonLibrary{
	
	static final Logger logger = Logger.getLogger(BookingFurnitureTest.class);
	
	@Test(groups = { "smoke" })
	public void bookItem() {
	try {
		logger.info("inside book item");
		ExtentTest log = setTestcaseName("BookingFurniture");
		FurnitureCategory category = new FurnitureCategory();
		BookingFurniturePage book = new BookingFurniturePage();
		PurchasePage purchase = new PurchasePage();
		HomePage home = new HomePage();
		//OrderDetailsPage order = new OrderDetailsPage();
		
		AddingToCartTest add = new AddingToCartTest();
		add.addItem();
		add.validateResponse();
		
		UpdateToCartTest update = new UpdateToCartTest();
		update.UpdateItem();
		update.validateResponse();
		
		home.click("ALLFURNITURE");
		category.click("SOFA");
		category.click("PRICE");
		category.click("CHAIRIMG");
		book.click("BookNOW");
		//order.click("PLACEORDER");
		//order.click("ORDERCONFIRM");
		//purchase.click("CASH");
		//purchase.click("PLACEORDER");
		//order.verifyOrder("Your Order is successfully placed");
		
		//BookFurnitureTest book = new BookFurnitureTest();
		
		//user.getUserId();
		//AddingToCart addCart = new AddingToCart();
		//addCart.searchProduct();
		//addCart.addItem(emailId, password);
		//addCart.validateResponse();
	} catch (Exception e) {
		e.printStackTrace();
	}
 }
	
}	


