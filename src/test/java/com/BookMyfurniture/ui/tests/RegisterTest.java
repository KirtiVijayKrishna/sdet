package com.BookMyfurniture.ui.tests;


import org.testng.Assert;
import org.testng.annotations.Test;

import com.BookMyfurniture.commonUtils.commonLibrary;
import com.BookMyfurniture.pages.HomePage;
import com.BookMyfurniture.pages.LoginPage;
import com.BookMyfurniture.pages.RegisterPage;

public class RegisterTest extends commonLibrary {

	@Test(groups = { "smoke" })
	public void testRegister() {
		try {
			String name = "Peter";
			String mobileNo = "9836789135";
			String emailId = "peter@gmail.com";
			String password = "Welcome@123";
			testlog = setTestcaseName("RegisterTest");
			
			testlog.info("Starting App");
			HomePage home = new HomePage();
			RegisterPage register = new RegisterPage();
			LoginPage login = new LoginPage(emailId, password);
			home.clickLink("SignIn");
			login.click("Create your Account");
			testlog.pass("Login Pass");
			boolean userAdded = register.fillForm(name, mobileNo, emailId,password, "Register");
			logger.info("user Registered ======" + userAdded);
			Assert.assertTrue(userAdded);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
