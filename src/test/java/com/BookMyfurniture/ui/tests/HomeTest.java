package com.BookMyfurniture.ui.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.BookMyfurniture.commonUtils.commonLibrary;
import com.BookMyfurniture.pages.HomePage;

public class HomeTest extends commonLibrary {

	@Test(groups= {"TC68567","smokeTest",})
	public void testClickSign() {
		HomePage home = new HomePage();
		home.clickLink("SIGN_IN");
		String actual = "Book My Furniture - QA(2.3.2)-Final";
		Assert.assertEquals(getDriver().getTitle(), actual);
	}

}
