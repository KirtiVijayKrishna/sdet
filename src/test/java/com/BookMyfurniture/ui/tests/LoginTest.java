package com.BookMyfurniture.ui.tests;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.BookMyfurniture.commonUtils.commonLibrary;
import com.BookMyfurniture.pages.BookingFurniturePage;
import com.BookMyfurniture.pages.FurnitureCategory;
import com.BookMyfurniture.pages.HomePage;
import com.BookMyfurniture.pages.LoginPage;
import com.BookMyfurniture.pages.PurchasePage;
import com.BookMyfurniture.commonUtils.RestController;
//import com.BookMyfurniture.rest.tests.UpdateToCartTest;
import com.BookMyfurniture.rest.tests.UserLoginTest;


public class LoginTest extends commonLibrary {
	
	 public static final Logger logger = Logger.getLogger(LoginTest.class);
	
	@Test(groups = { "smoke" }, dataProvider = "getSignUpData", priority = 1, description = "Sign up")
	public void testLogin(String name, String mobileNo, String emailId, String password) {
		try {
			testlog = setTestcaseName("LoginTest");
			testlog.info("Starting App");
			HomePage home = new HomePage();
			LoginPage login = new LoginPage(emailId,password);
			//FurnitureCategory category = new FurnitureCategory();
			//BookingFurniturePage book = new BookingFurniturePage();
			//PurchasePage purchase = new PurchasePage();
			//OrderDetailsPage order = new OrderDetailsPage();
			
			home.clickLink("SIGN_IN");
			login.click("Create your Account");
			testlog.pass("Login Pass");
			boolean userAdded = login.fillForm(name, mobileNo, emailId, password, "Register");
			logger.info("user added ======" + userAdded);
			Assert.assertTrue(userAdded);
			// Successfully Sign up
			login.fillForm(emailId, password);
			RestController.generateAccessToken(emailId, password);
			Assert.assertTrue(home.verifyUser(name));
			UserLoginTest user = new UserLoginTest(emailId,password);
			user.postUserLogin(emailId,password);
			if(user.verifyUser()) {
				logger.info("User logged in successfully");
				Assert.assertTrue(user.verifyUser());
			}
			
			//AddingToCartTest add = new AddingToCartTest(emailId, password);
			//add.addItem();
			//add.validateResponse();
			
			//UpdateToCartTest add = new UpdateToCartTest(emailId, password);
			//add.UpdateItem();
			//add.validateResponse();
			
			home.click("ALLFURNITURE");
			//category.click("SOFA");
			//category.click("PRICE");
			//category.click("CHAIRIMG");
			//book.click("BookNOW");
			//order.click("PLACEORDER");
			//order.click("ORDERCONFIRM");
			//purchase.click("CASH");
			//purchase.click("PLACEORDER");
			//order.verifyOrder("Your Order is successfully placed");
			
			//BookFurnitureTest book = new BookFurnitureTest();
			
			//user.getUserId();
			//AddingToCart addCart = new AddingToCart();
			//addCart.searchProduct();
			//addCart.addItem(emailId, password);
			//addCart.validateResponse();
				
			//BookFurnitureTest book = new BookFurnitureTest();
			//book.bookItem();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
}

