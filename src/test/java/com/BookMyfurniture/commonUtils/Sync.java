package com.BookMyfurniture.commonUtils;

import java.util.logging.Logger;

/**
	 * Utility class to aid in syncing on content in a page. .
	 * <p>
	 * 
	 * Sync provides methods that determine whether content is present in a very fast way, and also
	 * methods that loop over the fast methods that wait for a particular result.
	 * 
	 */
	public abstract class Sync {

	    /**
	     * IMPLICIT WAIT TIME 
	     */
	    public final static int DEFAULT_IMPLICIT_WAIT_SECONDS = 120;

	    /**
	     * Default Number of second to wait on elements when a page has already sync'ed.
	     */
	    public final static int DEFAULT_ELEMENT_WAIT_SECONDS = 5;

	    /**
	     * Logger to be used ONLY for sync information.
	     */
	    protected static Logger mSyncLogger = Logger.getLogger("sync." + Sync.class.getName());
	    

	    
}

	

