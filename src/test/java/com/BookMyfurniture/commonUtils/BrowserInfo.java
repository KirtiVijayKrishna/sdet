package com.BookMyfurniture.commonUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;


public class BrowserInfo {

    public WebDriver webDriver;
    
    public  BrowserSession browserSession = null ;
    
    public static final int SYNC_SECONDS = 20;
    
    private static final Logger logger = Logger.getLogger(BrowserInfo.class);
    
    
    /**
     * Closes the browser window.
     */
    public void closeBrowser() {
        if (browserSession.getDriver() != null) {
            WebDriver driver = browserSession.getDriver();
            try {
                driver.quit();
            } catch (WebDriverException wde) {
                logger.warn("Received WebDriverException during WebDriver quit()");
            }
        }
    }
    
    
   /**
    * Maximize the browser window.
    */
   public void maximize() {
       logger.info("maximize browser");
       browserSession.getDriver().manage().window().maximize();
   }

   /**
    * Get the current URL of the browser
    * @return
    */
   public String scrapeUrl() {
       return browserSession.getDriver().getCurrentUrl();
   }

  
  /* * Loads a URL in the browser.
   * 
   * @param url   */
  public static void loadUrl(String url, BrowserSession browserSession) {
      if (null == url) {
          logger.error("url cannot be null");
          return;
      }

      if (!url.startsWith("http")) {
          url = browserSession.getProductUrl();
      }
      //browserSession.getDriver().get(url);
  }


  /**
   * 
   * @return
   */
   public static BrowserType findBrowserType() {
      BrowserType browserType = BrowserType.FIREFOX;
      String type = BrowserSession.BROWSERTYPE;
      if (type.equalsIgnoreCase(BrowserType.CHROME.toString())) {
          browserType = BrowserType.CHROME;
      } else if (type.equalsIgnoreCase(BrowserType.FIREFOX.toString())) {
          browserType = BrowserType.FIREFOX;
      } 
      return browserType;

  }

  public void refresh() {
      browserSession.getDriver().navigate().refresh();
  }

  /**
   * clear the browser cookies.
   */
  public void clearCookies() {
      browserSession.getDriver().manage().deleteAllCookies();
  }

  
  public enum BrowserType {
      IE6("IE6"), IE7("IE7"), IE8("IE8"), IE9("IE9"), IE10("IE10"), IE11("IE11"), FIREFOX(
              "FIREFOX"), SAFARI("SAFARI"), CHROME("CHROME"), NONE("NONE");

      /**
       * @param type
       */
      private BrowserType(final String type) {
          this.type = type;
      }

      private final String type;

      /*
       * (non-Javadoc)
       * 
       * @see java.lang.Enum#toString()
       */
      @Override
      public String toString() {
          return type;
      }
  }

  /**
   * 
   * @return
   */
  /*public static BrowserType findBrowserType() {

      BrowserType browserType = BrowserType.FIREFOX;
      String type =
          KWMapUtil.getEventSysArg(BrowserSession.BROWSERTYPE, BrowserType.FIREFOX.toString());
      if (type.equalsIgnoreCase(BrowserType.CHROME.toString())) {
          browserType = BrowserType.CHROME;
      } else if (type.equalsIgnoreCase(BrowserType.SAFARI.toString())) {
          browserType = BrowserType.SAFARI;
      } else if (type.equalsIgnoreCase(BrowserType.FIREFOX.toString())) {
          browserType = BrowserType.FIREFOX;
      } else {
          browserType = determineIEVersion(browserType);
      }
      return browserType;

  }
*/

  /**
   * Search registry entries and finds IE version
   * 
   * @return
   */
  public static String getIEVersion() {

      try {
          Process p =
              Runtime.getRuntime().exec(
                  "reg query \"HKLM\\Software\\Microsoft\\Internet Explorer\" /v Version");
          BufferedReader stdInput =
              new BufferedReader(new InputStreamReader(p.getInputStream()), 8 * 1024);
          return stdInput.readLine();
      } catch (IOException e) {
          logger.warn("Unable to determine IEVERsion defaulting to IE8");
      } catch (Exception e) {
          logger.warn("Unable to determine IEVERsion defaulting to IE8");
      }
      return null;
  }

  /**
   * 
   * @param type
   * @return
   */
  public static BrowserType determineIEVersion(BrowserType type) {
      BrowserType result = BrowserType.IE8;
      if (BrowserType.IE7.toString().equals(type.toString())) { // anchor

          String ieVer = getIEVersion();

          if (null != ieVer) {
              if (ieVer.startsWith("6")) {
                  result = BrowserType.IE6;
              } else if (ieVer.startsWith("7")) {
                  result = BrowserType.IE7;
              } else if (ieVer.startsWith("8")) {
                  result = BrowserType.IE8;
              } else if (ieVer.startsWith("9")) {
                  result = BrowserType.IE9;
              }
          }
      }
      return result;
  }
}
