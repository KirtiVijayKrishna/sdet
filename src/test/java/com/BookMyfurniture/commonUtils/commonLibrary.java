package com.BookMyfurniture.commonUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class commonLibrary extends BrowserSession {

	protected static ExtentTest testlog;
	static ExtentReports report;
	static ExtentHtmlReporter htmlReport;
	public static String restUrl;

	public static HSSFWorkbook WBook = null;
	public static HSSFSheet WSheet = null;
	public static HSSFRow Row;
	public static HSSFCell cell;
	public static HSSFSheet Sheet = null;

	public static final Logger logger = Logger.getLogger(commonLibrary.class);
	
	public static String FilePath = System.getProperty("user.dir") + "\\src\\test\\resource\\Data1.xls";

	public void getUrl(String url) {
		getDriver().get(url);
	}

	public void enterText(String data, final By element) {
		try {
			if (waitForPage(element)) {
				getDriver().findElement(element).sendKeys(data);
				logger.info("enter data");
			}
		} catch (Exception Ex) {
			logger.error("Exception Occurred While Clicking The Element: " + Ex.getMessage());
		}

	}

	public void click(String button, final By element) {
		logger.info("Inside Click");
		try {
			if (waitForPage(element)) {
				getDriver().findElement(element).click();
				logger.info("element clicked");
			}
		} catch (Exception Ex) {
			logger.error("Exception Occurred While Clicking The Element: " + Ex.getMessage());
			testlog.fail("Click Failed"+ Ex.getMessage());
		}
	}

	public void submit(final By element) {
		logger.info("Inside Submit"+element);
		try {
			if (waitForPage(element)) {
				logger.info("Inside before "+element);
				getDriver().findElement(element).submit();
				logger.info("Inside last "+element);
			}
		} catch (Exception Ex) {
			Ex.printStackTrace();
			logger.error("Exception Occurred While Clicking The Element: " + Ex.getMessage());
		}
	}

	public boolean waitForPage(By locator) {
		WebDriverWait wait = new WebDriverWait(getDriver(), Sync.DEFAULT_ELEMENT_WAIT_SECONDS);
		boolean flag = true;
		try {
			wait.until(ExpectedConditions.elementToBeClickable(locator));
		} catch (Exception Ex) {
			flag = false;
			logger.error("Exception Occurred While Locating The Element: " + Ex.getMessage());
		}
		return flag;
	}

	public static boolean verifyTextMatch(String actualText, String expectedText) {
		boolean flag = false;
		try {
			logger.info("Actual Text From Application Web UI --> :: " + actualText);
			logger.info("Expected Text From Application Web UI --> :: " + expectedText);

			if (actualText.equals(expectedText)) {
				logger.info("### VERIFICATION TEXT MATCHED !!!");
				flag = true;
			} else {
				logger.error("### VERIFICATION TEXT DOES NOT MATCHED !!!");
			}

		} catch (Exception Ex) {
			logger.error("Exception Occurred While Verifying The Text Match: " + Ex.getMessage());
		}
		return flag;
	}

	public boolean verifyElementsLocated(ArrayList<By> arrayList) {
		boolean flag = true;
		try {
			for (By locator : arrayList) {
				WebElement element = getDriver().findElement(locator);
				if (waitForElementPresent(element)) {
					logger.info(element.toString() + ": element is displayed.");
				} else {
					logger.error(element.toString() + ": element isn't displayed.");
					flag = false;
				}
			}
		} catch (Exception Ex) {
			logger.error("Exception Occurred While Locating The Elements: " + Ex.getMessage());
		}
		return flag;
	}

	public boolean waitForElementPresent(WebElement element) {
		WebDriverWait wait = new WebDriverWait(getDriver(), Sync.DEFAULT_ELEMENT_WAIT_SECONDS);
		boolean flag = true;
		try {
			wait.until(ExpectedConditions.visibilityOf(element));
		} catch (Exception Ex) {
			flag = false;
			logger.error("Exception Occurred While Locating The Element: " + Ex.getMessage());
		}
		return flag;
	}
	
	public boolean isDisplayed(By by) {
		try {
			return getDriver().findElement(by).isDisplayed();
			
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	public static ExtentReports  initReports() {
		//String reportLocation= System.getProperty("user.dir") + "\\OutputReports\\ExtentReports.html";
		//Logger.info("reportPath========"+reportLocation);
		//String reportPath =InitSetup.get("reportLocation");
		//Logger.info("reportPath========"+reportPath);
		//String reportTitle = InitSetup.get("reportTitle");
		//Logger.info("reportTitle========"+reportTitle);
		htmlReport =new ExtentHtmlReporter(new File(System.getProperty("user.dir") + "/OutputReports/BookingFurniture.html")); 
		//htmlReport =new ExtentHtmlReporter(new File(System.getProperty("user.dir") + "BookingFurniture.html"));
		//htmlReport =new ExtentHtmlReporter("ExtentReport.html");
		
		report = new ExtentReports();
		report.attachReporter(htmlReport);
		logger.info("reportTitle========"+report);
		//htmlReport.config().setDocumentTitle(reportTitle);
		return report; 
	}

	public  void reportHandler(ExtentTest testLog,String info,String message) throws Exception {
		logger.info("Reporter step 1");
		if(info.equals("Fail")){
			testLog.log(Status.FAIL, "Test case Failed"+takeSnapShot(getDriver()));
			testLog.fail(MarkupHelper.createLabel("Test case Failed"+takeSnapShot(getDriver()), ExtentColor.RED));
			logger.info("Reporter step 2");
			//testLog.error(throwable.fillInStackTrace());
			try {
				logger.info("Reporter step 3");
				testLog.addScreenCaptureFromPath(takeSnapShot(getDriver()));
				}catch(IOException e) {e.printStackTrace();}
		}else if(info.equals("Pass")) {
			testLog.fail(MarkupHelper.createLabel("Test case Passed", ExtentColor.GREEN));
			logger.info("Reporter step 4");
			//testLog.error(throwable.fillInStackTrace());
			try {
				logger.info("Reporter step 5");
				testLog.addScreenCaptureFromPath(takeSnapShot(getDriver()));
			}catch(IOException e) {e.printStackTrace();}
		}
	}
	

	@BeforeClass
		public void connect() {
			try {
				Dao.connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
	
	@BeforeMethod(alwaysRun = true)
	public void init() {
		BrowserSession.initializeChromeDriver();
		InitSetup.initializeProjectProperty();
		restUrl = InitSetup.get("RestUrl");
		String openUrl = InitSetup.get("url");
		logger.info(openUrl);
		logger.info(restUrl);
		getUrl(openUrl);
		report =initReports();
		
		logger.info("report object==="+report);
	}

	@AfterMethod(alwaysRun = true)
	public void getResult(ITestResult result) throws Exception {
		if (result.getStatus() == ITestResult.FAILURE) {
			String screenShotPath = takeSnapShot(getDriver());
			testlog.log(Status.FAIL, MarkupHelper
					.createLabel(result.getName() + " Test case FAILED due to below issues:", ExtentColor.RED));
			testlog.fail(result.getThrowable());
			testlog.fail("Snapshot below: " + testlog.addScreenCaptureFromPath(screenShotPath, "LoginTest"));

		} else if (result.getStatus() == ITestResult.SKIP) {
			testlog.log(Status.SKIP, MarkupHelper
					.createLabel(result.getName() + " Test case SKIPPED due to below issues:", ExtentColor.GREY));
			testlog.skip(result.getThrowable());

		} else if (result.getStatus() == ITestResult.SUCCESS) {
			testlog.log(Status.PASS,
					MarkupHelper.createLabel(result.getName() + " Test case PASSED.", ExtentColor.GREEN));
		}
		quit();
	}
	public void quit() {
		closeBrowser();
		report.flush();
	}

	public void getExtentTest() {
		Long key = Thread.currentThread().getId();
	}


	public Long getTCKey(Long id) {
		return id + 1234567890;
	}

	public static ExtentTest setTestcaseName(String testcaseName) {
		//Long key = Thread.currentThread().getId();
		//Long tcKey = getTCKey(key);
		report=	initReports();
		testlog = report.createTest(testcaseName);
		logger.info("testlog ===="+testlog);
		return testlog;
	}

	public static HSSFSheet DataSheet(String FilePath, String SheetName) {
		File file = new File(FilePath);
		try {
			FileInputStream fis = new FileInputStream(file);
			WBook = new HSSFWorkbook(fis);
			WSheet = WBook.getSheet(SheetName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return WSheet;
	}

	public static String getCellData(String Sheet, int row, int col) {
		try {
			int index = WBook.getSheetIndex(Sheet);

			WSheet = WBook.getSheetAt(index);
			Row = WSheet.getRow(row);
			if (Row == null)
				return "";

			cell = Row.getCell(col);
			if (cell == null)
				return "";

			switch (cell.getCellType()) {
			case STRING:
				return cell.getStringCellValue();

			case BOOLEAN:
				return String.valueOf(cell.getBooleanCellValue());

			case BLANK:
				return "";

			case ERROR:
				return cell.getStringCellValue();

			case NUMERIC:
				return String.valueOf(cell.getNumericCellValue());

			default:
				return "Cell not found";

			}
		} catch (Exception e) {
			e.printStackTrace();
			return "row " + row + " or column " + col + " does not exist in xls";
		}
	}

	@DataProvider
	public static Object[][] getSignUpData() throws Exception {
		Object[][] userDetails;
		Sheet = DataSheet(FilePath, "Sheet3");
		int rowCount = Sheet.getLastRowNum();
		int colCount = Sheet.getRow(0).getLastCellNum();
		userDetails = new Object[rowCount][colCount];
		for (int rCnt = 1; rCnt <= rowCount; rCnt++) {
			for (int cCnt = 0; cCnt < colCount; cCnt++) {
				userDetails[rCnt - 1][cCnt] = getCellData("Sheet3", rCnt, cCnt);
			}
		}
		return userDetails;
	}
	
	public static String takeSnapShot(WebDriver webdriver) throws Exception{
		TakesScreenshot scrShot =((TakesScreenshot)webdriver);
				File srcFile=scrShot.getScreenshotAs(OutputType.FILE);
				String destFile = System.getProperty("user.dir") + "\\ScreenShots\\test.png";
				File target=new File(destFile);
				FileUtils.copyFile(srcFile, target);
				return destFile;
	}
	
	public boolean isElementPresent(By locator) {
	    try {
	        logger.info("Before isElementPresent::" + locator);
	    	WebDriverWait wait = new WebDriverWait(getDriver(), Sync.DEFAULT_ELEMENT_WAIT_SECONDS);
	        wait.until(ExpectedConditions.visibilityOf((WebElement) locator));
	        logger.info("After isElementPresent::" + locator);
	        return true;
	    } catch (Exception e) {
	        logger.info("Exception isElementPresent::" + locator);
	        return false;
	    }
	}

	public boolean isElementVisible(By locator) {
	    try{
	    	WebDriverWait wait = new WebDriverWait(getDriver(), Sync.DEFAULT_ELEMENT_WAIT_SECONDS);
	        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator));
	        return true;
	    } catch (Exception e) {
	        logger.info("Exception isElementPresent::" + locator);
	        return false;
	    }
	}

}
