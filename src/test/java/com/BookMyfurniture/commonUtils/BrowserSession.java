package com.BookMyfurniture.commonUtils;

import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.BookMyfurniture.commonUtils.BrowserInfo.BrowserType;


public  class BrowserSession  {

    private static WebDriver driver;

    public  String getProductUrl() {
		return null;
    	
    }
    
    /**
     * Logger for this class.
     */
    public static final Logger logger = Logger.getLogger(BrowserSession.class);

    public static final String BROWSERTYPE = "BROWSERTYPE";

    private static BrowserType browserType = null;

    public static BrowserType getBrowserType() {
        return browserType;
    }

    public static BrowserType findBrowserType() {
        if (null != browserType) {
            return browserType;
        }
        browserType = BrowserInfo.findBrowserType();
        return browserType;
    }
   
    

    /**
     * For use with Selenium, instantiate browser and driver.
     * @param server
     * @param type
     */
    public void initializeBrowserAndDriver(BrowserType type) {
        browserType = type;
        switch (type) {
            case IE6:
            case IE7:
            case IE8:
            case IE9:
            case IE10:
            case IE11:
                initializeIEDriver();
                break;
            case FIREFOX:
                initialiazeFFDriver();
                break;

            case CHROME:
                initializeChromeDriver();
                break;

            default:
                logger.warn("Unsupported browser type!! Default to Firefox!!");
                initialiazeFFDriver();

        }
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Sync.DEFAULT_IMPLICIT_WAIT_SECONDS, TimeUnit.SECONDS);

        driver.manage().deleteAllCookies();
        logger.warn("Cannot clear cookies");
    }

    /**
     * 
     */
    private void initialiazeFFDriver() {
        
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
    }

    /**
     * intialize chrome dirver
     */
    public static  void initializeChromeDriver() {
    	if(System.getProperty("browser").equalsIgnoreCase("Chrome")){
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        options.addArguments("--test-type"); 
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\Drivers\\chromedriver.exe");
        driver = new ChromeDriver(capabilities);
    }
    }

    /**
     * initialize Internet explorer driver
     */
    private void initializeIEDriver() {
    //    System.setProperty("webdriver.ie.driver", QCTOOLS + "/" + SELENIUM47 + "/"
      //      + "IEDriverServer32.exe");
        driver = new InternetExplorerDriver();
    }


    public boolean isAtLeastIEBrowser8() {
        return findBrowserType().equals(BrowserType.IE8);
    }

    public boolean isFFBrowser() {
        return findBrowserType().equals(BrowserType.FIREFOX);
    }

    public boolean isChromeBrowser() {
        return findBrowserType().equals(BrowserType.CHROME);
    }

    
    
    /**
     * Set Selenium Web Driver
     * 
     * @param driver the driver to set
     */
    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    /**
     * Obtain Selenium Web Driver reference
     * 
     * @return the driver
     */
    public WebDriver getDriver() {
        return driver;
    }
    
    /**
     * Closes the browser window.
     */
    public  void closeBrowser() {
        if (getDriver() != null) {
            WebDriver driver = getDriver();
            
            try {
                driver.quit();
                logger.info("Browser Closed");
            } catch (WebDriverException wde) {
                logger.warn("Received WebDriverException during WebDriver quit()");
            }
        }
    }
    
}	    




