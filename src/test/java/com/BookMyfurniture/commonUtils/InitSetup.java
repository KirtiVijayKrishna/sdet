package com.BookMyfurniture.commonUtils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

public class InitSetup {


public static final Logger logger = Logger.getLogger(InitSetup.class);
	
	static Properties dataproperties;
	
	public InitSetup() {}{
	}

	public static void initializeProjectProperty() {
		dataproperties = new Properties();
		String currentDir = System.getProperty("user.dir");
		logger.info("Curent Directory ===="+currentDir);
		String datapath = System.getProperty("user.dir") + "\\src\\test\\resource\\config.properties";
		logger.info("Actual Directory ===="+datapath);
		try {
			FileInputStream f = new FileInputStream(datapath);
			dataproperties.load(f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.getMessage();
		}
	}
	
	
	public static String get(String key) {
		try {
			return dataproperties.getProperty(key);
		} catch (Exception e) {
			logger.info(e.getMessage());
			return '!' + key + '!';
		}
	}
	

}
