package com.BookMyfurniture.commonUtils;
	
	import java.util.Map;
	import org.apache.log4j.Logger;
	import io.restassured.RestAssured;
	import io.restassured.http.ContentType;
	import io.restassured.response.Response;
	import io.restassured.specification.RequestSpecification;

	public class RestBase {

		public static RequestSpecification httpRequest;
		
		public static Response response;
		
		public static final Logger logger = Logger.getLogger(RestBase.class);
		
		public static  void setBaseURI(String baseURI) {
			RestAssured.baseURI=baseURI;
		}
		
		public static  void restSetBaseURI() {
			RestAssured.baseURI=null;
		}
		
		public static void setBasePath(String basePath) {
			RestAssured.basePath = basePath;
		}
		
		public static  void restSetBasePath() {
			RestAssured.basePath=null;
		}
		
		public void setContentType(ContentType type ) {
			httpRequest.given().contentType(type);
		}
		
		public static Response Get(String uri) {
			httpRequest = RestAssured.given().contentType(ContentType.JSON); 
			response = httpRequest.get(uri);
			return response;
		}	

		public Response postWithFormData(String authHeader, String authHeaderValue, Map<String, String> formData,String endPointURL) {
			Response response = null;
			response = RestAssured.given().baseUri("http://okmry52647dns.eastus.cloudapp.azure.com:8089").header(authHeader, authHeaderValue).header("content-type",ContentType.URLENC).formParams(formData).post(endPointURL).then().log().all().extract().response();
			return response;
		}
		
		}
		



