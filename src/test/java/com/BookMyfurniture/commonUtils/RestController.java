package com.BookMyfurniture.commonUtils;

	import java.util.HashMap;

	import org.apache.log4j.Logger;
	import org.json.JSONObject;

	import com.BookMyfurniture.ui.tests.LoginTest;

	import io.restassured.response.Response;

	public class RestController {
		
		 public static final Logger logger = Logger.getLogger(LoginTest.class);
		 
		public static String generateAccessToken(String userName, String password) {
			// MAP OF FORM DATA;username=john@gmail.com&password=Welcome@123&grant_type=password
			HashMap<String, String> map = new HashMap<>();
			map.put("username",userName);
			map.put("password", password);
			map.put("grant_type", "password");
			Response res = new RestBase().postWithFormData("Authorization", "Basic amFtYWxDbGllbnRIZXJlOmphbWFsU2VjcmV0SGVyZQ==", map,"/oauth/token");
			org.json.JSONObject object = new JSONObject(res.asString());
			String token= object.getString("access_token");
			logger.info("response data"+object.getString("access_token"));
			return token;
		}
		
		
	}		
		



